package mundo;

public class Main {
    public static void main(String[] args) {

        System.out.println("Hello world!");

        Singleton singleton1 = Singleton.darSingleton();
        Singleton singleton2 = Singleton.darSingleton();
        System.out.println(singleton1.hashCode());
        System.out.println(singleton2.hashCode());

    }

}
