package mundo;

public class Singleton {

    private static Singleton singleton;

    private Singleton() {

    }

    public static Singleton darSingleton() {

        if (singleton == null){

            singleton = new Singleton();

        }

        return singleton;

    }

}
